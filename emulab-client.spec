Name: emulab-client
URL: https://gitlab.flux.utah.edu/emulab/emulab-devel
Version: 1.44.10
Release: 5%{?dist}
License: AGPLv3
Source: %{name}-%{version}.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires: autoconf
BuildRequires: make
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: libtirpc-devel
BuildRequires: openssl-devel
BuildRequires: libpcap-devel
BuildRequires: sudo
BuildRequires: python3
BuildRequires: zlib-devel
BuildRequires: boost-devel
BuildRequires: boost-graph
BuildRequires: wget
BuildRequires: curl
BuildRequires: flex
BuildRequires: git
BuildRequires: device-mapper-devel
BuildRequires: perl-English
BuildRequires: pubsub-devel
#BuildRequires: 
Requires: coreutils
Requires: procps-ng
Requires: util-linux
Requires: tcsh
Requires: bash
Requires: zsh
Requires: grep
Requires: nfs-utils-coreos
Requires: rpcbind
Requires: bzip2
Requires: gzip
Requires: xz
Requires: unzip
Requires: ca-certificates
Requires: cpio
Requires: device-mapper
Requires: mdadm
Requires: lvm2
Requires: usbutils
Requires: bind-utils
Requires: dosfstools
Requires: iptables-nft
Requires: file
Requires: findutils
Requires: gawk
Requires: sed
Requires: tar
Requires: traceroute
Requires: git
Requires: rsync
Requires: hdparm
Requires: systemd
Requires: systemd-udev
Requires: sudo
Requires: passwd
Requires: perl
Requires: hostname
Requires: iputils
Requires: lshw
Requires: net-tools
Requires: iproute
Requires: iproute-tc
Requires: tcpdump
Requires: bridge-utils
#Requires: vconfig
Requires: ethtool
Requires: wget
Requires: curl
Requires: parted
Requires: gdisk
Requires: ntpstat
Requires: openssl
Requires: iscsi-initiator-utils
Requires: smartmontools
Requires: libtirpc
Requires: libpcap
Requires: device-mapper-libs
Requires: pubsub
#Requires: 
Summary: The Emulab clientside software.

%description
This software provides the Emulab client side that configures
testbed nodes in an Emulab cluster.

%package grub2-x64
Summary: The Emulab clientside software - grub EFI/BIOS configuration.
Requires: grub2-pc
Requires: grub2-efi-x64
BuildArch: noarch

%description grub2-x64
An appropriate grub configuration for Emulab cluster nodes that supports
simultaneous EFI and BIOS grub installs.
#The Ubuntu packages containing
#the automatic grub-install hooks are grub-efi-amd64 and grub-pc, but those
#packages cannot be present on an installed system at once.  Consequently,
#this package recommends grub-efi-amd64 and relies on its hooks to
#reinstall grub when updated (and sets the appropriate debconf vars to do
#so).  However, it also includes its own trigger on /usr/lib/grub/i386-pc
#that reinstalls grub to the MBR and BIOS boot partition when that dir is
#updated, e.g. by an update to grub-pc-bin.  This is less than ideal, but
#preserves the relevant behavior of grub-pc for our configuration.

%package rsyslog
Summary: The Emulab clientside software - rsyslog configuration.
BuildArch: noarch
Requires: rsyslog

%description rsyslog
An appropriate rsyslog configuration for Emulab cluster nodes.

%package chrony
Summary: The Emulab clientside software - chrony configuration.
BuildArch: noarch
Requires: chrony

%description chrony
An appropriate chrony configuration for Emulab cluster nodes.

%package NetworkManager
Summary: The Emulab clientside software - NetworkManager control net support.
BuildArch: noarch
Requires: NetworkManager
Conflicts: emulab-client-networkd

%description NetworkManager
An appropriate NetworkManager configuration that allows Emulab
cluster nodes to find their control network.

%package networkd
Summary: The Emulab clientside software - systemd-networkd control net support.
BuildArch: noarch
Requires: systemd-networkd
Conflicts: emulab-client-NetworkManager

%description networkd
An appropriate systemd-networkd configuration that allows Emulab
cluster nodes to find their control network.

%package legacy
Summary: The Emulab clientside software - legacy path support.
BuildArch: noarch
Requires: emulab-client

%description legacy
Support for Emulab legacy executable paths.


%prep
%setup -q -n emulab-client-%{version}
#%patch -p1 -b .patch-name

%build
autoconf
mkdir -p obj
pushd obj
../configure --prefix=/usr --libexecdir=/usr/libexec \
    --libdir=/usr/lib/emulab --mandir=/usr/man
make client

%install
pushd obj
make DESTDIR=%{buildroot} \
     NETWORKTARGETS="'systemd-networkd NetworkManager'" \
     client-install
popd
install -d -m 755 -o root -g root %{buildroot}/lib/systemd/system
#install -d -m 755 -o root -g root %{buildroot}/lib/systemd/system-generators
mv %{buildroot}/etc/systemd/system/* %{buildroot}/lib/systemd/system/
#mv %{buildroot}/etc/systemd/system-generators/* %{buildroot}/lib/systemd/system-generators/
rm -rf %{buildroot}/etc/systemd/system
#rm -rf %{buildroot}/etc/systemd/system-generators
install -d -m 755 -o root -g root %{buildroot}/usr/share/emulab/
#install -m 644 -o root -g root clientside/tmcc/linux/grub-defaults %{buildroot}/usr/share/emulab/
mv %{buildroot}/etc/default/grub %{buildroot}/usr/share/emulab/grub-defaults
sed -i -e 's|python$|python3|' %{buildroot}/usr/bin/geni-get
mv %{buildroot}/etc/chrony.conf %{buildroot}/usr/share/emulab/
rm -f /etc/testbed
cp git-version %{buildroot}/etc/emulab/version
rm -rf %{buildroot}/etc/dhclient* %{buildroot}/etc/dhcp*
rm -rf %{buildroot}/etc/sysconfig/network*
%post
# Install passwd master files if they don't already exist.
for f in passwd group shadow gshadow ; do
    if [ ! -e /etc/emulab/$f ]; then
	cp -p /etc/$f /etc/emulab/$f
    fi
done

%post grub2-x64
grubchanged=0
grep -qi emulab /etc/default/grub
if [ ! $? -eq 0 ]; then
    if [ ! -e /etc/default/grub.dist -a -e /etc/default/grub ]; then
	mv /etc/default/grub /etc/default/grub.dist
    fi
    cp -p /usr/share/emulab/grub-defaults /etc/default/grub
    grubchanged=1
fi
if [ $grubchanged -eq 1 ]; then
    grub2-mkconfig -o /boot/grub2/grub.cfg
fi

%post chrony
if [ ! -e /etc/chrony.conf.dist ]; then
    cp -p /etc/chrony.conf /etc/chrony.conf.dist
fi
cp -p /usr/share/emulab/chrony.conf /etc/chrony.conf

%post legacy
make_symlink() {
    target="$1"
    name="$2"
    if [ -e "$name" ]; then
	if [ -L "$name" ]; then
	    sval=`readlink "$name"`
	    if [ ! "$sval" = "$target" ]; then
		echo "ERROR: $name link already exists and is not our target $target, cannot overwrite"
		exit 1
	    fi
	else
	    echo "ERROR: $name already exists, cannot overwrite"
	    exit 1
	fi
    else
	ln -s "$target" "$name"
    fi
}
make_symlink ../../libexec/emulab /usr/local/etc/emulab
for f in create-versioned-image create-image imagezip imageunzip ; do
    make_symlink ../../bin/$f /usr/local/bin/$f
done

%postun legacy
if [ $1 -eq 1 ]; then
    exit 0
fi
unmake_symlink() {
    target="$1"
    name="$2"
    [ -e "$name" -a -L "$name" -a `readlink "$name"` = "$target" ] \
	&& rm -f "$name"
}
unmake_symlink ../../libexec/emulab /usr/local/etc/emulab
for f in create-versioned-image create-image imagezip imageunzip ; do
    unmake_symlink ../../bin/$f /usr/local/bin/$f
done

%files
/etc/systemd/system-generators/systemd-fstab-generator
/lib/systemd/system/emulab-fstab-fixup.service
/lib/systemd/system/testbed.service
/lib/systemd/system/tbprepare.service
/lib/systemd/system/multi-user.target.wants/emulab-fstab-fixup.service
/lib/systemd/system/multi-user.target.wants/testbed.service
/etc/emulab/paths.*
/etc/emulab/version
/etc/emulab/uses-systemd
/etc/sudoers.d/99-emulab
/etc/ssh/sshd_config.d/20-emulab.conf
/usr/man/man1/*
/usr/man/man8/*
/usr/bin/*
/usr/lib/emulab/emulabclient.py
/usr/libexec/emulab/*
/usr/testbed/bin/tevc
/usr/testbed/bin/emulab-sync
/usr/testbed/bin/mkextrafs
/usr/testbed/lib
/usr/sbin/*
/var/emulab/*
/etc/selinux/config
/etc/dracut.conf.d/01-dist.conf
%exclude /usr/libexec/emulab/*NetworkManager*
%exclude /usr/lib/emulab/BasicXMLRPCServers.py
%exclude /usr/lib/emulab/libxmlrpc.pm
%exclude /etc/testbed
%exclude /etc/emulab/passwd
%exclude /etc/emulab/group
%exclude /etc/emulab/shadow
%exclude /etc/emulab/gshadow
%exclude /etc/emulab/hosts
%exclude /etc/emulab/update
%exclude /etc/emulab/supfile
%exclude /etc/ld.so.conf.d/emulab.conf
%exclude /etc/logrotate.d/syslog
%exclude /etc/rc.d/rc.local
%exclude /etc/rc.local
%exclude /lib/systemd/system/chronyd.service.d
%exclude /lib/systemd/system/multi-user.target.wants/chronyd.service
%exclude /usr/libexec/emulab/chronystart
%exclude /root/.cvsup/auth

# This value should be a regexp body that can be used by
# `brp-mangle-shebangs`, which does the following:
#
# echo "$path" | grep -q -E "$exclude_files" && continue
%global __brp_mangle_shebangs_exclude_from emulabclient.py|sslxmlrpc_client.py|script_wrapper.py

%files grub2-x64
/usr/share/emulab/grub-defaults

%files rsyslog
/etc/rsyslog.d/40-emulab.conf

%files chrony
/usr/share/emulab/chrony.conf
/usr/libexec/emulab/chronystart
/lib/systemd/system/chronyd.service.d/10-emulab-chronystart.conf

%files NetworkManager
/usr/libexec/emulab/emulab-NetworkManager.sh
/usr/libexec/emulab/emulab-NetworkManager-udev-helper.sh
/usr/libexec/emulab/emulab-udev-settle-NetworkManager.sh
/lib/systemd/system/emulab-NetworkManager@.service
/lib/systemd/system/emulab-udev-settle-NetworkManager.service
/lib/systemd/system/NetworkManager.service.requires
/etc/udev/rules.d/01-disable-bluefield-nvme.rules
/etc/udev/rules.d/99-emulab-NetworkManager.rules
/etc/NetworkManager/conf.d/90-emulab.conf

%files networkd
/usr/libexec/emulab/emulab-networkd.sh
/usr/libexec/emulab/emulab-networkd-udev-helper.sh
/usr/libexec/emulab/emulab-udev-settle-networkd.sh
/lib/systemd/system/emulab-networkd@.service
/lib/systemd/system/emulab-udev-settle-networkd.service
/lib/systemd/system/systemd-networkd.service.requires
/lib/systemd/system/systemd-networkd.socket.requires
/etc/udev/rules.d/01-disable-bluefield-nvme.rules
/etc/udev/rules.d/99-emulab-networkd.rules

%files legacy

%changelog
* Fri Nov 22 2024 David M. Johnson <johnsond@flux.utah.edu> - 1.44.13-1
- Update to dc99ab4 (1.44.13-1).
* Wed Jul 03 2024 David M. Johnson <johnsond@flux.utah.edu> - 1.44.12-1
- Update to 456d1a973 (1.44.12-1).
* Mon Apr 22 2024 David M. Johnson <johnsond@flux.utah.edu> - 1.44.10-5
- Additional xmlrpc install fixes.
* Mon Apr 22 2024 David M. Johnson <johnsond@flux.utah.edu> - 1.44.10-4
- xmlrpc install fixes.
* Wed Apr 17 2024 David M. Johnson <johnsond@flux.utah.edu> - 1.44.10-3
- Work around upstream exclude macro changes.
* Wed Apr 17 2024 David M. Johnson <johnsond@flux.utah.edu> - 1.44.10-2
- Work around upstream python shebang checks.
* Thu Apr 11 2024 David M. Johnson <johnsond@flux.utah.edu> - 1.44.10-1
- Update to 0ae5cef6 (1.44.10-1).
* Wed Apr 19 2023 David M. Johnson <johnsond@flux.utah.edu> - 1.44.4-4
- Minor fix to scriptlets for upgrade.
* Tue Apr 18 2023 David M. Johnson <johnsond@flux.utah.edu> - 1.44.4-3
- Minor fix to previous release.
* Tue Apr 18 2023 David M. Johnson <johnsond@flux.utah.edu> - 1.44.4-2
- Minor fix to previous release.
* Tue Apr 18 2023 David M. Johnson <johnsond@flux.utah.edu> - 1.44.4-1
- Pull in new chrony configuration.
* Mon Apr 17 2023 David M. Johnson <johnsond@flux.utah.edu> - 1.44.3-2
- Minor fix to emulab-client-legacy postinst scriptlet.
* Sun Apr 16 2023 David M. Johnson <johnsond@flux.utah.edu> - 1.44.3-1
- 1.44.3 release (liblocstorage fixes).
* Fri Apr 07 2023 David M. Johnson <johnsond@flux.utah.edu> - 1.44.2-1
- 1.44.2 release.
* Mon Jan 02 2023 David M. Johnson <johnsond@flux.utah.edu> - 1.44.1+20221209+a887a885-1
- CI testing.
* Sat Aug 20 2022 David M. Johnson <johnsond@flux.utah.edu> - 1.44.1-1
- Initial packaging.
